package main

import (
	"context"
	"fmt"

	"github.com/open-policy-agent/opa/ast"
	"github.com/open-policy-agent/opa/rego"
)

func evalRules(module string, compiler *ast.Compiler, input interface{}, ctx context.Context) (resources []map[string]interface{}, err error) {

	logger := WithContextLogger(ctx)

	query := fmt.Sprintf(`data.%s.resources = resources`, module)

	r := rego.New(
		rego.Compiler(compiler),
		rego.Query(query),
		rego.Input(input),
	)

	rs, err := r.Eval(ctx)
	if err != nil {
		logger.Errorw("Failed to evaluate query",
			"query", query,
			"error", err)
		return resources, err
	}

	logger.Debugw("Query result",
		"query", query,
		"result", rs)

	for _, r := range rs {
		for _, rr := range r.Bindings["resources"].([]interface{}) {
			switch rr := rr.(type) {
			case []interface{}:
				for _, r := range rr {
					resources = append(resources, r.(map[string]interface{}))
				}
			case interface{}:
				resources = append(resources, rr.(map[string]interface{}))
			}
		}
	}
	return resources, nil

}
