# Stargate

Stargate is a generic CI/CD control plane based on Tekton and OPA.

Rules are written in [Rego](https://www.openpolicyagent.org/docs/latest/how-do-i-write-policies/) and are loaded by Stargate.

Side-effects are delegated to a [Kubernetes](https://kubernetes.io/) cluster with [Tekton](https://github.com/tektoncd/pipeline) deployed.

The main goal is to make every use-case needed implementable as configuration without modifying Stargate itself.

The kinds of artifact you will need to create are : Rego rules, Tekton manifests and OCI images, hopefully the community will share all of them for most commons needs.

## Webhooks mode

Inputs are standardized HTTP requests (webhooks) from differents services (like Github/Gitlab/Slack/Trello/etc...).

Outputs are Kubernetes objects.

Example with file `webhooks.rego`:

```rego
package webhooks

resources[t] {
  input.headers["X-Github-Event"][_] == "issues"
  input.payload.action == "opened"
  t := {
    "apiVersion": "tekton.dev/v1alpha1",
    "kind": "TaskRun",
    "metadata": {
      "generateName": "gh-add-labels-"
    },
    "spec": {
      "taskRef": {
        "name": "gh-add-labels",
      },
      "inputs": {
        "params": [
            {"name": "labels", "value": ["need-approval"]},
            {"name": "issue_id", "value": input.payload.issue.id}
        ]
      }
    }
  }
}
```

When Stargate start it will load `webhooks.rego` and accept requests on `/webhooks`.

Then it will run the task `gh-add-labels` when any input request matches rego rules.

### Rego modules

Currently each `rego` file correspond to an `endpoint` exposed by Stargate:

* `xx.rego` -> `/xx`
* `xy.rego` -> `/xy`

When a request is received all `resources` rules of the `rego` module are evaluated.
Theses rules must return valid kubernetes resource.

Multiple rules can match and in this case multiple resources will be created.
A rule can also return a list of resources.

## Supported resources

Currently Stargate can create the following resources:

* TaskRun
* PipelineRun
* Namespace
* ServiceAccount
* ClusterRoleBinding

## Running Stargate

Stargate is meant to be run in a kubernetes cluster. `rego` modules can be
provided through a ConfigMap. When the ConfigMap changes Stargate will reload
`rego` modules automatically.

First deploy [Tekton pipelines](https://github.com/tektoncd/pipeline) to the cluster:

```sh
$ kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml
```

A [deployment](./deployment.yaml) of Stargate is provided. It exposes `/endpoint1` as an example.

You can deploy it with [ko](https://github.com/google/ko):

```sh
$ KO_DOCKER_REPO=xxxx ko apply -f ./deployment.yaml
```

Then the stargate pod should be running:

```sh
$ kubectl logs -n stargate stargate-9f57f766b-qhgkn
{"level":"info","ts":1573578783.5438633,"caller":"stargate/main.go:115","msg":"No GITHUB_SECRET_TOKEN provided. Github validation is disabled!"}
{"level":"info","ts":1573578783.543906,"caller":"stargate/main.go:119","msg":"No GITLAB_SECRET_TOKEN provided. Gitlab validation is disabled!"}
{"level":"info","ts":1573578783.5439165,"caller":"stargate/main.go:126","msg":"No KUBECONFIG provided, using in-cluster config."}
{"level":"info","ts":1573578783.5441782,"caller":"stargate/main.go:189","msg":"Not a rego module: /modules/..2019_11_12_17_12_08.964466893"}
```

You can then test the example endpoint:

```sh
$ kubectl port-forward -n stargate svc/stargate 8080
$ curl -d '{"msg": "hello from stargate"}' -H 'Content-Type: application/json' http://localhost:8080/endpoint1
[{"metadata":{"name":"endpoint1-task-run-9cphb","generateName":"endpoint1-task-run-","namespace":"default","selfLink":"/apis/tekton.dev/v1alpha1/namespaces/default/taskruns/endpoint1-task-run-9cphb","uid":"654e9f44-6859-4abb-916e-b5c169e7d977","resourceVersion":"3181","generation":1,"creationTimestamp":"2019-11-12T17:29:14Z"},"spec":{"inputs":{},"outputs":{},"serviceAccountName":"","taskSpec":{"steps":[{"name":"test","image":"nixery.dev/shell","command":["bash"],"args":["-c","echo hello from stargate"],"resources":{},"imagePullPolicy":"IfNotPresent"}]},"timeout":"1h0m0s","podTemplate":{}},"status":{"podName":""}}]
```

Stargate returns the list of created resources.

```sh
$ kubectl get pods
NAME                                  READY   STATUS      RESTARTS   AGE
endpoint1-task-run-9cphb-pod-972bc1   0/1     Completed   0          99s
$ kubectl logs endpoint1-task-run-9cphb-pod-972bc1
hello from stargate
```
