package endpoint1

resources[t] {
  t := {
        "kind": "Namespace",
        "apiVersion": "v1",
        "metadata": {
            "name": "endpoint2",
        },
  }
}

resources[t] {
  t := [
    {
        "kind": "Namespace",
        "apiVersion": "v1",
        "metadata": {
            "name": "endpoint1",
        },
    },
    {
        "kind": "ServiceAccount",
        "apiVersion": "v1",
        "metadata": {
            "name": "endpoint1-sa",
            "namespace": "endpoint1",
        }
    },
    {
        "apiVersion": "rbac.authorization.k8s.io/v1",
        "kind": "ClusterRoleBinding",
        "metadata": {
            "name": "endpoint1-crb",
        },
        "roleRef": {
            "apiGroup": "rbac.authorization.k8s.io",
            "kind": "ClusterRole",
            "name": "edit"
        },
        "subjects": [
            {
                "kind": "ServiceAccount",
                "name": "endpoint1-sa",
                "namespace": "endpoint1"
            }
        ]
    },
    {
      "apiVersion": "tekton.dev/v1alpha1",
      "kind": "TaskRun",
      "metadata": {
        "generateName": "endpoint1-task-run-",
        "namespace": "endpoint1",
      },
      "serviceAccount": "endpoint1-sa",
      "spec": {
        "taskSpec": {
          "steps": [
            {
              "name": "test",
              "image": "nixery.dev/shell",
              "imagePullPolicy": "IfNotPresent",
              "command": ["bash"],
              "args": ["-c", sprintf("echo %s", [input.payload.msg])],
            }
          ],
        },
      }
    }
  ]
}
