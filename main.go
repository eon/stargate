/*
 Copyright 2019 Orange

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"

	arg "github.com/alexflint/go-arg"
	"github.com/fsnotify/fsnotify"
	"github.com/google/go-github/github"
	"github.com/google/uuid"
	"github.com/open-policy-agent/opa/ast"
	v1alpha1 "github.com/tektoncd/pipeline/pkg/client/clientset/versioned/typed/pipeline/v1alpha1"
	"go.uber.org/zap"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

const (
	envGithubSecret = "GITHUB_SECRET_TOKEN"
	envGitlabSecret = "GITLAB_SECRET_TOKEN"
	envKubeconfig   = "KUBECONFIG"
)

var (
	logger            *zap.SugaredLogger
	errModuleNotFound = errors.New("Module not found")
)

type stargateModule struct {
	path     string
	realpath string
	compiler *ast.Compiler
	sync.RWMutex
}

func (m stargateModule) filename() string {
	return filepath.Base(m.path)
}

func (m stargateModule) name() string {
	return strings.Split(m.filename(), ".")[0]
}

func (m *stargateModule) update() (err error) {
	logger.Debugf("Updating module %s", m.filename())
	realpath, err := filepath.EvalSymlinks(m.path)
	if err != nil {
		return
	}
	data, err := ioutil.ReadFile(realpath)
	if err != nil {
		return
	}
	compiler, err := ast.CompileModules(map[string]string{m.filename(): string(data)})
	if err != nil {
		return
	}
	m.Lock()
	defer m.Unlock()
	m.compiler = compiler
	m.realpath = realpath
	return
}

func (m *stargateModule) getCompiler() *ast.Compiler {
	m.RLock()
	defer m.RUnlock()
	return m.compiler
}

func NewstargateModule(path string) (m *stargateModule, err error) {
	m = &stargateModule{
		path: path,
	}
	err = m.update()
	return
}

type Stargate struct {
	modulesPath  string
	modules      map[string]*stargateModule
	githubSecret []byte
	gitlabSecret string
	tektonClient *v1alpha1.TektonV1alpha1Client
	k8sClient    *kubernetes.Clientset
	sync.RWMutex
}

func NewStargate(modulesPath string, kubeconfig string) *Stargate {
	githubSecret := os.Getenv(envGithubSecret)
	if githubSecret == "" {
		logger.Infof("No %s provided. Github validation is disabled!", envGithubSecret)
	}
	gitlabSecret := os.Getenv(envGitlabSecret)
	if gitlabSecret == "" {
		logger.Infof("No %s provided. Gitlab validation is disabled!", envGitlabSecret)
	}
	var (
		config *rest.Config
		err    error
	)
	if kubeconfig == "" {
		logger.Infof("No %s provided, using in-cluster config.", envKubeconfig)
		config, err = rest.InClusterConfig()
		if err != nil {
			logger.Fatal(err)
		}
	} else {
		config, err = clientcmd.BuildConfigFromFlags("", kubeconfig)
		if err != nil {
			logger.Fatal(err)
		}
	}
	tektonClient, err := v1alpha1.NewForConfig(config)
	if err != nil {
		logger.Fatal(err)
	}
	k8sClient, err := kubernetes.NewForConfig(config)
	if err != nil {
		logger.Fatal(err)
	}
	compiler, err := ast.CompileModules(map[string]string{"healthz.rego": `
		package healthz

		resources[n] {
		  n := {
			"kind": "Namespace",
			"apiVersion": "v1",
			"metadata": {
				"name": "default"
			}
		  }
		}
	`})
	if err != nil {
		logger.Fatal(err)
	}
	healthzModule := &stargateModule{
		compiler: compiler,
	}

	return &Stargate{
		modules: map[string]*stargateModule{
			"healthz": healthzModule,
		},
		modulesPath:  modulesPath,
		githubSecret: []byte(githubSecret),
		gitlabSecret: gitlabSecret,
		tektonClient: tektonClient,
		k8sClient:    k8sClient,
	}
}

func (s *Stargate) unloadModule(module *stargateModule) (err error) {
	s.Lock()
	defer s.Unlock()
	delete(s.modules, module.name())
	logger.Debugf("Unloaded %s", module.filename())
	return nil
}

func (s *Stargate) loadModule(modulePath string) (err error) {
	m, err := NewstargateModule(modulePath)
	if err != nil {
		return err
	}
	s.Lock()
	defer s.Unlock()
	s.modules[m.name()] = m
	logger.Debugf("Loaded %s", m.filename())
	return nil
}

func checkModuleFile(path string) bool {
	if filepath.Ext(path) == ".rego" && !strings.HasSuffix(path, "_test.rego") {
		return true
	}
	return false
}

func (s *Stargate) loadModules() (err error) {
	files, err := ioutil.ReadDir(s.modulesPath)
	if err != nil {
		return
	}
	for _, file := range files {
		path := filepath.Join(s.modulesPath, file.Name())
		if checkModuleFile(path) {
			if err := s.loadModule(path); err != nil {
				return err
			}
		} else {
			logger.Debugf("Not a rego module: %s", path)
		}
	}
	return
}

func (s *Stargate) getModuleByRealpath(realpath string) (*stargateModule, bool) {
	for _, m := range s.modules {
		if m.realpath == realpath || m.realpath == fmt.Sprintf("%s/%s", realpath, m.filename()) {
			return m, true
		}
	}
	return &stargateModule{}, false
}

func (s *Stargate) watchModules() {
	const writeOrCreateMask = fsnotify.Write | fsnotify.Create
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		logger.Fatal(err)
	}
	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				logger.Debugf("FS event:", event)
				if event.Op&fsnotify.Remove == fsnotify.Remove {
					if m, ok := s.getModuleByRealpath(event.Name); ok {
						if _, err := filepath.EvalSymlinks(m.path); err != nil {
							// Module actually removed
							err := s.unloadModule(m)
							if err != nil {
								logger.Error(err)
							}
						} else {
							// In cluster mode with configmap
							// Module realpath changed
							err := m.update()
							if err != nil {
								logger.Error(err)
							}
						}
					} else {
						logger.Debugf("No module found for %s", event.Name)
					}
				}
				if event.Op&writeOrCreateMask != 0 {
					if checkModuleFile(event.Name) {
						err := s.loadModule(event.Name)
						if err != nil {
							logger.Error(err)
						}
					}
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				logger.Error(err)
			}
		}
	}()
	logger.Debugf("Watching %s for changes", s.modulesPath)
	err = watcher.Add(s.modulesPath)
	if err != nil {
		logger.Fatal(err)
	}
	<-done
}

func (s *Stargate) getModuleCompiler(name string) (compiler *ast.Compiler, err error) {
	s.RLock()
	defer s.RUnlock()
	if module, ok := s.modules[name]; ok {
		return module.getCompiler(), nil
	}
	return compiler, errModuleNotFound
}

func (s *Stargate) requestID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("X-GitHub-Delivery") != "" {
			id := github.DeliveryID(r)
			ctx := context.WithValue(r.Context(), "RequestID", id)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			id, err := uuid.NewRandom()
			if err != nil {
				next.ServeHTTP(w, r)
			} else {
				ctx := context.WithValue(r.Context(), "RequestID", id.String())
				next.ServeHTTP(w, r.WithContext(ctx))
			}
		}
	})
}

func (s *Stargate) validateGithubRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("X-GitHub-Delivery") != "" && len(s.githubSecret) > 0 {
			payload, err := github.ValidatePayload(r, s.githubSecret)
			if err != nil {
				http.Error(w, err.Error(), http.StatusUnauthorized)
				return
			}
			ctx := context.WithValue(r.Context(), "Payload", payload)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func (s *Stargate) validateGitlabRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// https://gitlab.com/gitlab-org/gitlab/issues/19367
		if len(s.gitlabSecret) > 0 {
			if token := r.Header.Get("X-Gitlab-Token"); token != "" && token != s.gitlabSecret {
				http.Error(w, "Wrong gitlab token", http.StatusUnauthorized)
				return
			}
		}
		next.ServeHTTP(w, r)
	})
}

func (s *Stargate) getPayload(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// r.Body may have already be read by previous handlers
		ctxPayload := r.Context().Value("Payload")
		if ctxPayload == nil {
			payload, err := ioutil.ReadAll(r.Body)
			if err != nil {
				http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
				return
			}
			if len(payload) == 0 {
				payload = []byte("{}")
			}
			ctx := context.WithValue(r.Context(), "Payload", payload)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			next.ServeHTTP(w, r)
		}
	})
}

func (s *Stargate) evalRequest() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error

		logger := WithContextLogger(r.Context())
		reqID := r.Context().Value("RequestID")
		payload := r.Context().Value("Payload").([]byte)

		module := r.URL.Path[1:]
		compiler, err := s.getModuleCompiler(module)
		if err != nil {
			http.Error(w, "Invalid endpoint", http.StatusBadRequest)
			return
		}

		headers := r.Header
		if reqID != nil {
			headers.Add("X-Stargate-Request-Id", reqID.(string))
		}

		logger.Debugw("Got request",
			"endpoint", r.URL.Path,
			"payload", string(payload),
			"headers", headers)

		var unmarshaledPayload interface{}
		err = json.Unmarshal(payload, &unmarshaledPayload)
		if err != nil {
			logger.Errorw("Failed to decode request payload",
				"error", err)
			http.Error(w, "Failed to decode request payload", http.StatusInternalServerError)
			return
		}
		event := map[string]interface{}{
			"headers": headers,
			"payload": unmarshaledPayload,
		}

		resources, err := evalRules(module, compiler, event, r.Context())
		if err != nil {
			http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
			return
		}
		created, err := s.createResources(resources, r.Context())
		if err != nil {
			http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		createdJSON, _ := json.Marshal(created)
		if len(created) > 0 {
			w.WriteHeader(http.StatusCreated)
			w.Write(createdJSON)
		} else {
			w.WriteHeader(http.StatusNoContent)
		}
	})
}

func main() {

	var args struct {
		ModulesPath string `arg:"--modules-path,env:STARGATE_MODULES_PATH,required"`
		Debug       bool   `arg:"-d,env:STARGATE_DEBUG"`
		Kubeconfig  string `arg:"env:KUBECONFIG"`
	}
	arg.MustParse(&args)

	var l *zap.Logger
	if args.Debug {
		l, _ = zap.NewDevelopment()
	} else {
		l, _ = zap.NewProduction()
	}
	logger = l.Sugar()
	defer logger.Sync()

	s := NewStargate(args.ModulesPath, args.Kubeconfig)
	err := s.loadModules()
	if err != nil {
		logger.Fatal(err)
	}
	go s.watchModules()

	router := http.NewServeMux()
	router.Handle("/", s.requestID(s.validateGitlabRequest(s.validateGithubRequest(s.getPayload(s.evalRequest())))))
	router.Handle("/healthz", s.getPayload(s.evalRequest()))
	server := &http.Server{
		Addr:    ":8080",
		Handler: router,
	}
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		logger.Fatal(err)
	}

}
