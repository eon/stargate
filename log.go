package main

import (
	"context"

	"go.uber.org/zap"
)

type ContextLogger struct {
	logger *zap.SugaredLogger
	ctx    context.Context
}

func (c ContextLogger) DefaultKeysAndValues() []interface{} {
	return []interface{}{
		"request_id", c.ctx.Value("RequestID"),
	}
}

func (c ContextLogger) Infow(msg string, keysAndValues ...interface{}) {
	if c.logger == nil {
		return
	}
	keysAndValues = append(keysAndValues, c.DefaultKeysAndValues()...)
	c.logger.Infow(msg, keysAndValues...)
}

func (c ContextLogger) Debugw(msg string, keysAndValues ...interface{}) {
	if c.logger == nil {
		return
	}
	keysAndValues = append(keysAndValues, c.DefaultKeysAndValues()...)
	c.logger.Debugw(msg, keysAndValues...)
}

func (c ContextLogger) Errorw(msg string, keysAndValues ...interface{}) {
	if c.logger == nil {
		return
	}
	keysAndValues = append(keysAndValues, c.DefaultKeysAndValues()...)
	c.logger.Errorw(msg, keysAndValues...)
}

// WithContextLogger returns the global logger associated with a context
func WithContextLogger(ctx context.Context) ContextLogger {
	reqID := ctx.Value("RequestID")
	// No RequestID is generated for /healthz endpoint to skip logging on this endpoint
	if reqID == nil {
		return ContextLogger{ctx: ctx}
	} else {
		return ContextLogger{logger: logger, ctx: ctx}
	}
}
