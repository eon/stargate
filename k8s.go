package main

import (
	"context"
	"encoding/json"
	"errors"
	"sort"

	tektonv1alpha1 "github.com/tektoncd/pipeline/pkg/apis/pipeline/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	errResourceNoMetadata = errors.New("No metadata in resource")
	errResourceNoKind     = errors.New("Resource has no kind")
)

type k8sResource struct {
	r map[string]interface{}
}

func (r k8sResource) Kind() (string, error) {
	if kind, ok := r.r["kind"]; ok {
		return kind.(string), nil
	} else {
		return "", errResourceNoKind
	}
}

func (r k8sResource) Namespace() (string, error) {
	if metadata, ok := r.r["metadata"]; !ok {
		return "", errResourceNoMetadata
	} else {
		if ns, ok := metadata.(map[string]interface{})["namespace"].(string); !ok {
			return "default", nil
		} else {
			return ns, nil
		}
	}
}

func (r k8sResource) Weigth() int {
	kind, _ := r.Kind()
	switch kind {
	case "Namespace":
		return 50
	case "ServiceAccount":
		return 40
	case "ClusterRoleBinding":
		return 30
	default:
		return 0
	}
}

func (r k8sResource) MarshalJSON() ([]byte, error) {
	return json.Marshal(r.r)
}

type byWeigth []k8sResource

func (w byWeigth) Len() int {
	return len(w)
}

func (w byWeigth) Less(i, j int) bool {
	return w[i].Weigth() > w[j].Weigth()
}

func (w byWeigth) Swap(i, j int) {
	w[i], w[j] = w[j], w[i]
}

func (s Stargate) createResources(resources []map[string]interface{}, ctx context.Context) (createdResources []interface{}, err error) {

	logger := WithContextLogger(ctx)

	rs := []k8sResource{}
	for _, resource := range resources {
		rs = append(rs, k8sResource{r: resource})
	}
	sort.Sort(byWeigth(rs))

	for _, r := range rs {
		namespace, err := r.Namespace()
		if err != nil {
			return nil, err
		}
		kind, err := r.Kind()
		if err != nil {
			return nil, err
		}
		switch kind {
		case "Namespace":
			var ns corev1.Namespace
			jsonResource, _ := json.Marshal(r)
			err = json.Unmarshal(jsonResource, &ns)
			if err != nil {
				logger.Errorw("Failed to decode resource",
					"resource_kind", kind,
					"resource", jsonResource,
					"error", err)
				return nil, err
			}
			_, err := s.k8sClient.CoreV1().Namespaces().Get(ns.ObjectMeta.Name, metav1.GetOptions{})
			if err != nil {
				created, err := s.k8sClient.CoreV1().Namespaces().Create(&ns)
				if err != nil {
					logger.Errorw("Failed to create resource",
						"resource_kind", kind,
						"resource", jsonResource,
						"error", err)
					return nil, err
				}
				logger.Debugw("Created resource",
					"resource_kind", kind,
					"resource_name", created.ObjectMeta.Name)
				createdResources = append(createdResources, created)
			} else {
				logger.Debugw("Resource already exists",
					"resource_kind", kind,
					"resource_name", ns.ObjectMeta.Name)
			}
		case "ServiceAccount":
			var sa corev1.ServiceAccount
			jsonResource, _ := json.Marshal(r)
			err = json.Unmarshal(jsonResource, &sa)
			if err != nil {
				logger.Errorw("Failed to decode resource",
					"resource_kind", kind,
					"resource", jsonResource,
					"error", err)
				return nil, err
			}
			_, err := s.k8sClient.CoreV1().ServiceAccounts(namespace).Get(sa.ObjectMeta.Name, metav1.GetOptions{})
			if err != nil {
				created, err := s.k8sClient.CoreV1().ServiceAccounts(namespace).Create(&sa)
				if err != nil {
					logger.Errorw("Failed to create resource",
						"resource_kind", kind,
						"resource", jsonResource,
						"error", err)
					return nil, err
				}
				logger.Debugw("Created resource",
					"resource_kind", kind,
					"resource_name", created.ObjectMeta.Name)
				createdResources = append(createdResources, created)
			} else {
				logger.Debugw("Resource already exists",
					"resource_kind", kind,
					"resource_name", sa.ObjectMeta.Name)
			}
		case "ClusterRoleBinding":
			var crb rbacv1.ClusterRoleBinding
			jsonResource, _ := json.Marshal(r)
			err = json.Unmarshal(jsonResource, &crb)
			if err != nil {
				logger.Errorw("Failed to decode resource",
					"resource_kind", kind,
					"resource", jsonResource,
					"error", err)
				return nil, err
			}
			_, err := s.k8sClient.RbacV1().ClusterRoleBindings().Get(crb.ObjectMeta.Name, metav1.GetOptions{})
			if err != nil {
				created, err := s.k8sClient.RbacV1().ClusterRoleBindings().Create(&crb)
				if err != nil {
					logger.Errorw("Failed to create resource",
						"resource_kind", kind,
						"resource", jsonResource,
						"error", err)
					return nil, err
				}
				logger.Debugw("Created resource",
					"resource_kind", kind,
					"resource_name", created.ObjectMeta.Name)
				createdResources = append(createdResources, created)
			} else {
				logger.Debugw("Resource already exists",
					"resource_kind", kind,
					"resource_name", crb.ObjectMeta.Name)
			}
		case "TaskRun":
			var taskRun tektonv1alpha1.TaskRun
			jsonResource, _ := json.Marshal(r)
			err = json.Unmarshal(jsonResource, &taskRun)
			if err != nil {
				logger.Errorw("Failed to decode resource",
					"resource_kind", kind,
					"resource", jsonResource,
					"error", err)
				return nil, err
			}
			created, err := s.tektonClient.TaskRuns(namespace).Create(&taskRun)
			if err != nil {
				logger.Errorw("Failed to create resource",
					"resource_kind", kind,
					"resource", jsonResource,
					"error", err)
				return nil, err
			}
			logger.Debugw("Created resource",
				"resource_kind", kind,
				"resource_name", created.ObjectMeta.Name)
			createdResources = append(createdResources, created)
		case "PipelineRun":
			var pipelineRun tektonv1alpha1.PipelineRun
			jsonResource, _ := json.Marshal(r)
			err = json.Unmarshal(jsonResource, &pipelineRun)
			if err != nil {
				logger.Errorw("Failed to decode resource",
					"resource_kind", kind,
					"resource", jsonResource,
					"error", err)
				return nil, err
			}
			created, err := s.tektonClient.PipelineRuns(namespace).Create(&pipelineRun)
			if err != nil {
				logger.Errorw("Failed to create resource",
					"resource_kind", kind,
					"resource", jsonResource,
					"error", err)
				return nil, err
			}
			logger.Debugw("Created resource",
				"resource_kind", kind,
				"resource_name", created.ObjectMeta.Name)
			createdResources = append(createdResources, created)
		default:
			logger.Infow("Resource not supported",
				"resource_kind", kind)
			continue
		}
	}

	return createdResources, nil
}
